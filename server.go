package main

import (
	"fmt"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/Sirupsen/logrus"
	"golang.org/x/net/webdav"
	goftp "gopkg.in/dutchcoders/goftp.v1"
)

type Server struct {
	Handler *webdav.Handler
	config  *Config
}

type AuthMiddleware struct {
	Server *Server
}

func NewServer(config *Config) *Server {
	return &Server{
		config: config,
	}
}

func (a *AuthMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	user, ok := a.Server.HasUser(r)
	if !ok {
		logrus.Infof("(n/a) %s: %s", r.Method, r.URL.Path)
		w.Header().Set("WWW-Authenticate", `Basic realm="webdav-relay"`)
		http.Error(w, "Authorization Failed", http.StatusUnauthorized)
		return
	}
	if !user.IsAllowedToCallMethod(r.Method) {
		logrus.Warnf("(%s) Method %s not allowed", user.Login, r.Method)
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}
	logrus.Infof("(%s) %s: %s", user.Login, r.Method, r.URL.Path)
	if a.Server.Handler != nil {
		a.Server.Handler.ServeHTTP(w, r)
	}
}

func (s *Server) HasUser(r *http.Request) (*User, bool) {
	login, password, ok := r.BasicAuth()
	if !ok {
		if s.config.Anonymous != nil {
			return s.config.Anonymous, true
		}
		return nil, false
	}
	for _, user := range s.config.Users {
		if user.Login == login && user.Password == password {
			return user, true
		}
	}
	return nil, false
}

func (s *Server) Configure() error {
	fs := &Filesystem{}
	webdavFs, err := fs.Mount(s.config.UploadDirectory)
	if err != nil {
		return err
	}
	s.Handler = &webdav.Handler{
		FileSystem: webdavFs,
		LockSystem: webdav.NewMemLS(),
		Logger:     s.PostProcessHandler,
	}
	return nil
}

func (s *Server) RelayToFilesystem(relay *Relay, filename string) error {
	from := path.Join(s.config.UploadDirectory, filename)
	to := path.Join(relay.Directory, filepath.Base(filename))
	return CopyFile(from, to)
}

func (s *Server) RelayToSleep(relay *Relay) error {
	time.Sleep(relay.Duration)
	return nil
}

func (s *Server) RelayToFTP(relay *Relay, filename string) error {
	from := path.Join(s.config.UploadDirectory, filename)
	to := path.Join(relay.Directory, filepath.Base(filename))
	ftp, err := goftp.Connect(fmt.Sprintf("%s:%d", relay.Hostname, relay.Port))
	if err != nil {
		return err
	}
	defer ftp.Close()
	if err := ftp.Login(relay.Login, relay.Password); err != nil {
		return err
	}
	file, err := os.Open(from)
	if err != nil {
		return err
	}
	if err := ftp.Stor(to, file); err != nil {
		return err
	}
	return nil
}

func (s *Server) RelayFileFor(filename string, user *User) error {
	from := path.Join(s.config.UploadDirectory, filename)
	for _, relay := range user.Relays {
		to := path.Join(relay.Directory, filepath.Base(filename))
		switch relay.Type {
		case RELAY_PAUSE:
			logrus.Infof("(%s) Sleep for a %s", user.Login, relay.Duration.String())
			s.RelayToSleep(relay)
		case RELAY_COPY:
			logrus.Infof("(%s) Copy file %s to %s", user.Login, from, to)
			err := s.RelayToFilesystem(relay, filename)
			if err != nil {
				logrus.Errorf("(%s) %s", user.Login, err)
				return err
			}
		case RELAY_FTP:
			logrus.Infof("(%s) Copy file %s on FTP server (%s:%d) to %s", user.Login, from, relay.Hostname, relay.Port, to)
			err := s.RelayToFTP(relay, filename)
			if err != nil {
				logrus.Errorf("(%s) %s", user.Login, err)
				return err
			}
		}
	}
	if !user.SaveOriginalFile {
		from := path.Join(s.config.UploadDirectory, filename)
		err := os.Remove(from)
		if err != nil {
			logrus.Errorf("(%s) %s", user.Login, err)
			return nil
		}
		logrus.Infof("(%s) Source file %s deleted", user.Login, from)
	}
	return nil
}

func (s *Server) PostProcessHandler(r *http.Request, err error) {
	user, _ := s.HasUser(r)
	if err != nil {
		logrus.Errorf("(%s) %s", user.Login, err)
		return
	}
	switch r.Method {
	case http.MethodPut:
		if err := s.RelayFileFor(r.URL.Path, user); err != nil {
			logrus.Errorf("(%s) %s", user.Login, err)
		}
	}
}

func (s *Server) Run() error {
	http.Handle("/", &AuthMiddleware{s})
	return http.ListenAndServe(s.config.ListenAddress, nil)
}
