# Webdav relay server

Currently I'm using this daemon for uploading screenshots on FTP server bypassing locked port.

## Configuration

```yaml
---
listenAddress: 127.0.0.1:8099
uploadDirectory: tmp
users:
  - login: leo
    password: pass
    saveOriginalFile: true
    relays:
      - type: ftp
        login: login
        password: password
        hostname: ftp.server.com
        directory: htdocs/
      - type: copy
        directory: backup_directory/
anonymous:
  saveOriginalFile: false
  notAllowedMethods:
    - DELETE
    - MKCOL
    - MOVE
    - COPY
    - PROPPATCH
  relays:
    - type: ftp
      login: login
      password: password
      hostname: ftp.server.com
      directory: htdocs/
```
