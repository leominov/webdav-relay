package main

import (
	"os"
	"path/filepath"

	"golang.org/x/net/webdav"
)

type Filesystem struct{}

func (f *Filesystem) Mount(source string) (webdav.FileSystem, error) {
	if source == "" {
		source = "."
	}
	if s, err := filepath.Abs(source); err == nil {
		source = s
	}
	return webdav.Dir(source), nil
}

func (f *Filesystem) CreateFS(source string) error {
	if source == "" {
		source = "."
	}
	if s, err := filepath.Abs(source); err == nil {
		source = s
	}
	return os.Mkdir(source, 0755)
}
