package main

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

const (
	RELAY_FTP   = "ftp"
	RELAY_COPY  = "copy"
	RELAY_PAUSE = "pause"
)

var (
	SupportedRelayTypes = map[string]bool{
		RELAY_FTP:   true,
		RELAY_COPY:  true,
		RELAY_PAUSE: true,
	}
)

type Relay struct {
	Type      string        `yaml:"type"`
	Login     string        `yaml:"login"`
	Password  string        `yaml:"password"`
	Hostname  string        `yaml:"hostname"`
	Port      int           `yaml:"port"`
	Directory string        `yaml:"directory"`
	Duration  time.Duration `yaml:"duration"`
}

func (r *Relay) SetDefaults() {
	r.Type = strings.ToLower(r.Type)
	switch r.Type {
	case RELAY_FTP:
		if r.Port == 0 {
			r.Port = 21
		}
	}
}

func (r *Relay) Validate() error {
	if _, ok := SupportedRelayTypes[r.Type]; !ok {
		return fmt.Errorf("Unsupported relay type: %s", r.Type)
	}
	switch r.Type {
	case RELAY_FTP:
		if len(r.Directory) == 0 {
			return errors.New("Directory must be specified for FTP relay")
		}
		if len(r.Hostname) == 0 {
			return errors.New("Hostname must be specified for FTP relay")
		}
	case RELAY_COPY:
		if len(r.Directory) == 0 {
			return errors.New("Directory must be specified for Copy relay")
		}
	case RELAY_PAUSE:
		if r.Duration.Nanoseconds() == 0 {
			return errors.New("Duration must be specified for Pause relay")
		}
	}
	return nil
}
