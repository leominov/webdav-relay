package main

import (
	"flag"

	"github.com/Sirupsen/logrus"
)

var (
	configFileFlag = flag.String("config", "config.yaml", "Configuration file")
	logLevelFlag   = flag.String("log-level", "info", "Logging level")
)

func main() {
	flag.Parse()
	level, err := logrus.ParseLevel(*logLevelFlag)
	if err != nil {
		logrus.Fatalf("Logger: %v", err)
	}
	logrus.SetLevel(level)
	config, err := NewConfigFromFile(*configFileFlag)
	if err != nil {
		logrus.Fatal(err)
	}
	logrus.Infof("Starting webdav relay server on %s...", config.ListenAddress)
	server := NewServer(config)
	if err := server.Configure(); err != nil {
		logrus.Fatal(err)
	}
	logrus.Fatal(server.Run())
}
