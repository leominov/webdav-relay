package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/Sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	UploadDirectory string  `yaml:"uploadDirectory"`
	ListenAddress   string  `yaml:"listenAddress"`
	Users           []*User `yaml:"users"`
	Anonymous       *User   `yaml:"anonymous"`
}

func NewConfigFromFile(filename string) (*Config, error) {
	config := new(Config)
	configBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fmt.Errorf("File %s does not exist", filename)
		}
		if os.IsPermission(err) {
			return nil, fmt.Errorf("Can't read file %s, check permission", filename)
		}
		return nil, err
	}
	err = yaml.Unmarshal(configBytes, &config)
	if err != nil {
		return nil, err
	}
	if len(config.UploadDirectory) == 0 {
		return nil, errors.New("Upload directory must be specified")
	}
	if len(config.Users) == 0 && config.Anonymous == nil {
		return nil, errors.New("User list or anonymous must be specified")
	}
	config.SetDefaults()
	for _, user := range config.Users {
		err := user.ValidateAsAuthorized()
		if err != nil {
			return nil, err
		}
	}
	if config.Anonymous != nil {
		err := config.Anonymous.ValidateAsAnonymous()
		if err != nil {
			return nil, err
		}
	}
	return config, nil
}

func (c *Config) SetDefaults() {
	if len(c.ListenAddress) == 0 {
		c.ListenAddress = "127.0.0.1:8099"
		logrus.Warnf("Using default listen address: %s", c.ListenAddress)
	}
	for _, user := range c.Users {
		user.SetDefaults()
	}
	if c.Anonymous != nil {
		logrus.Warn("Accessible for anonymous users")
		c.Anonymous.SetDefaults()
		c.Anonymous.Login = "anon"
	}
}
