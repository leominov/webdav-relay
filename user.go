package main

import (
	"errors"
	"fmt"
	"strings"
)

type User struct {
	Login             string   `yaml:"login"`
	Password          string   `yaml:"password"`
	SaveOriginalFile  bool     `yaml:"saveOriginalFile"`
	Relays            []*Relay `yaml:"relays"`
	NotAllowedMethods []string `yaml:"notAllowedMethods"`
}

func (u *User) IsAllowedToCallMethod(method string) bool {
	if len(u.NotAllowedMethods) == 0 {
		return true
	}
	for _, notAllowedMethod := range u.NotAllowedMethods {
		if method == strings.ToUpper(notAllowedMethod) {
			return false
		}
	}
	return true
}

func (u *User) ValidateAsAuthorized() error {
	if len(u.Login) == 0 {
		return errors.New("Found user without login")
	}
	if len(u.Password) == 0 {
		return fmt.Errorf("Found user %s without password", u.Login)
	}
	for _, relay := range u.Relays {
		err := relay.Validate()
		if err != nil {
			return err
		}
	}
	return nil
}

func (u *User) ValidateAsAnonymous() error {
	for _, relay := range u.Relays {
		err := relay.Validate()
		if err != nil {
			return err
		}
	}
	return nil
}

func (u *User) SetDefaults() {
	for _, relay := range u.Relays {
		relay.SetDefaults()
	}
}
